﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TabuadaDoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int numeroDigitado;
            int i = 1;
            Console.Write("Digite um número: ");
            numeroDigitado = int.Parse(Console.ReadLine());

            do
            {
                Console.WriteLine(numeroDigitado + " x " + i + " = " + (numeroDigitado * i));
                i++;
            }
            while (i <= 10);

           Console.ReadKey();
        }
    }
}
